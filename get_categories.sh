#!/bin/bash
#
# gitlab CLI utility is gitlab-gem: https://www.rubydoc.info/gems/gitlab/4.6.1
# syntax page/docs at https://www.rubydoc.info/gems/gitlab/4.6.1/Gitlab/Client/Issues

# Make sure proper parameters are defined.
if [ $# -lt 1 ]; then
   echo "Error: categories list filename required."
   echo "Usage: $(basename $0) <filename>"
   exit 1
fi

ruby="`which ruby`"

# Define the ruby script inline and pass it into ruby
eval $ruby - << RUBYSCRIPT

require 'yaml'

# Read in the yaml file
filedata = YAML.load(File.read("$1"))

filedata.each_key do |key|
   print  filedata[key]["name"], ", ", filedata[key]["stage"], "\n"
end

RUBYSCRIPT
