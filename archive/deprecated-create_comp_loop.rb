#!/bin/bash
#
# gitlab CLI utility is gitlab-gem: https://www.rubydoc.info/gems/gitlab/4.6.1
# syntax page/docs at https://www.rubydoc.info/gems/gitlab/4.6.1/Gitlab/Client/Issues

# Set endpoint credentials
. ./gitlab-config

ruby="`which ruby`"

eval $ruby $@ << RUBYSCRIPT

require 'gitlab/api'

STAGE = 'manage'
LIST_NAME = "#{STAGE} stage"
PROJECT_ID = '921868'

endpoint = 'https://gitlab.com/api/v4'
private_token = 'CxZxxtTNdaYdEe_5LQXH'

comparisons = Hash[ \
"Basecamp" => "Issue Boards", \
"Dapulse" => "Issue Boards", \
"Freedcamps" => "Issue Boards", \
"Podio" => "Issue Boards", \
"Teamwork Projects" => "Issue Boards", \
"TrackVia" => "Issue Boards", \
"Wrike" => "Issue Boards / Portfolio Management", \
"Airbrake" => "Issue Tracking", \
"Axosoft" => "Issue Tracking", \
"BMC Remedy" => "Issue Tracking", \
"Bugsnag" => "Issue Tracking", \
"Bugzilla" => "Issue Tracking", \
"Countersoft" => "Issue Tracking", \
"Fog Creek Software" => "Issue Tracking", \
"FogBugz" => "Issue Tracking", \
"Inflectra Corporation" => "Issue Tracking", \
"JetBrains" => "Issue Tracking", \
"Pivotal Tracker" => "Issue Tracking", \
"QA Symphony" => "Issue Tracking", \
"Zoho Sprints" => "Issue Tracking / Issue Boards", \
"HP Application Lifecycle Management" => "Portfolio Management", \
"IBM Rational Team Concert" => "Portfolio Management", \
"Plutora" => "Portfolio Management", \
"Atlassian Jira Service Desk" => "Service desk", \
"CA Service Desk Manager" => "Service desk", \
"Freshdesk" => "Service desk", \
"ServiceNow" => "Service desk", \
"Zendesk" => "Service desk" \
]

# Configure gitlab API for our server and credentials
Gitlab.configure do |config|
   config.endpoint      = "endpoint"      # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
   config.private_token = "private_token" # private or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
   # Optional
   # config.user_agent   = 'Custom User Agent'  # user agent, default: 'Gitlab Ruby Gem [version]'
   # config.sudo         = 'user'               # username for sudo mode, default: nil
end

comparisons.each do |key, value|
   print "Creating issue for ", key, " for category ", value, "\n"


#   gitlab create_issue $PROJECT_ID "${tool} comparison" "{description: 'Create a comparison page for ${tool} in stage $STAGE', labels: '${LIST_NAME}, comparison, Product Marketing'}"
end

RUBYSCRIPT
