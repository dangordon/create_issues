#!/bin/bash
#
# gitlab CLI utility is gitlab-gem: https://www.rubydoc.info/gems/gitlab/4.6.1
# syntax page/docs at https://www.rubydoc.info/gems/gitlab/4.6.1/Gitlab/Client/Issues

# Make sure proper parameters are defined.
if [ $# -lt 1 ]; then
   echo "Error: comparison list filename required."
   echo "Usage: $(basename $0) <filename>"
   exit 1
fi

# Set endpoint credentials
. ./gitlab-config

ruby="`which ruby`"

# Define the ruby script inline and pass it into ruby
eval $ruby - << RUBYSCRIPT

require 'gitlab'
require 'yaml'

# Read in the yaml file
filedata = YAML.load(File.read("$1"))
comparisons = filedata["comparisons"]

LIST_NAME = "Open"
PROJECT_ID = '10364921'
endpoint = 'https://gitlab.com/api/v4'
private_token = 'CxZxxtTNdaYdEe_5LQXH'

# Configure gitlab API for our server and credentials
Gitlab.configure do |config|
   config.endpoint      = "#{endpoint}"      # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
   config.private_token = "#{private_token}" # private or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
end

g = Gitlab.client(endpoint: endpoint, private_token: private_token)

comparisons.each do |key, value|
   options = {
      description: "Categorie(s): #{value}",
      labels: "#{LIST_NAME}"
   }
   print "Creating issue for ", key, " for category ", value, "\n"
   #g.create_issue(project, title, options = {}) ⇒ Gitlab::ObjectifiedHash 
   g.create_issue(PROJECT_ID, "Create #{key} comparison page", options)
end

RUBYSCRIPT
